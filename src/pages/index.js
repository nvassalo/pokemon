import React, { useState, useEffect } from 'react'
import { Router, Link, useLocation } from '@reach/router'
import useStore from '../store/useStore'
import Layout from '../components/_layout'

import Home from '../components/home'
import Pokemon from '../components/pokemon'

// markup
const IndexPage = () => {
  return (
    <Layout>
      <Router>
        <Home path="/" />
        <Pokemon path="/pokemon/:name" />
      </Router>
    </Layout>
  )
}


export default IndexPage
