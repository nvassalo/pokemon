import React, { useEffect, useState } from 'react'
import useStore from '../store/useStore'


const Pokemon = ({ name }) => {
    const [pokemon, setPokemon] = useState({})
    const [loaded, setLoaded] = useState()

    useEffect(() => {
        queryPokemon().then(el => {
            console.log(el)
            setPokemon(el)
            setLoaded(true)
        })
    }, [])

    const queryPokemon = async () => {
        const query = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
        console.log(query)
        const data = await query.json()
        return data
        // console.log(data)
    }
    return (
        <div className="hero">
            <div className="hero-body">
                <header>
                    {loaded
                        ?
                        <>
                            <h2>{pokemon.name}, <br />
                                <small>id: {pokemon.id}</small>
                            </h2>
                            <img src={pokemon.sprites.front_default} alt={`${pokemon.name} from the front`} />
                        </>
                        :
                        'loading'
                    }
                </header>
            </div>
        </div>

    )
}

export default Pokemon