/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import '../scss/main.scss'


// import useStore from '../utils/useStore'


const Layout = ({ children, className }) => {
    // const scrollRef = useRef()
    // useEffect(() => {
    //   new Scroll(scrollRef)
    // }, [])
    // useEffect(() => {
    //   const scroll = new LocomotiveScroll({
    //     el: el.current,
    //     smooth: true,
    //     inertia: 1
    //   })
    //   return function () {
    //     scroll.destroy()
    //     if (el) el.current.className = ''
    //     if (el) el.current.dataset.scroll = ''
    //   }
    //   // return scroll.destroy;
    // }, [el])

    return (
        <>
            {/* <Header /> */}
            <main>
                {/* <Model /> */}
                {children}
            </main>
            {/* <Footer /> */}
        </>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired
}

export default Layout
