import React, { useEffect, useState } from 'react'
import { Link } from '@reach/router'
import useStore from '../store/useStore'


const Home = () => {
    const listOfPokemon = useStore('listOfPokemon')
    const setListOfPokemon = useStore('setListOfPokemon')
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        loadFromAPI().
            then((data) => {
                setListOfPokemon(data.results)
                setLoaded(true)
            })
        // return () => {
        //     cleanup
        // }
    }, [])

    const loadFromAPI = async () => {
        const fetchPokemonAPI = await fetch('https://pokeapi.co/api/v2/pokemon?limit=100&offset=200');
        const json = await fetchPokemonAPI.json()
        return json
    }

    return (
        loaded
            ?
            <ul>
                {listOfPokemon.map(el => {
                    return (
                        <li key={el.id}>
                            <Link to={`/pokemon/${el.name}`}>
                                My name is {el.name}
                            </Link>
                        </li>
                    )
                })}
            </ul >
            : 'Loading'
    )

}

export default Home