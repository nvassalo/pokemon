import create from 'zustand'
import { devtools } from 'zustand/middleware'

const useStore = create(devtools((set) => ({
    listOfPokemon: [],
    setListOfPokemon: (v) => set((state) => ({ listOfPokemon: v })),
})))

export default useStore
