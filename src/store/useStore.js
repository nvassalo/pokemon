import useStore from './store.js'


export default (key) => useStore((state) => state[key])